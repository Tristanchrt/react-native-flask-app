import flask
from flask_pymongo import PyMongo
from routes.itemRoute import *
from routes.userRoute import *

class Server:
    def __init__(self):
        self.app = None
        self.mongo = None

    def create_app(self):
        self.app = flask.Flask(__name__)
        self.app.config["DEBUG"] = True
        self.app.config["MONGO_URI"] = "mongodb+srv://root:root@cluster0.sgqw3.gcp.mongodb.net/test"
        self.mongo = PyMongo(self.app)

    def create_routes(self):
        ItemRoute(self.mongo).routes(self.app)
        UserRoute(self.mongo).routes(self.app)

    def start_server(self):
        self.create_app()
        self.create_routes()
        self.app.run(host="192.168.1.140")