from flask import request, jsonify
from functools import wraps
from utils.token import verifTokenJWT


def authentication(api_method):
    @wraps(api_method)

    def check_api_key(*args, **kwargs):
        try:
            tokenCheck = request.headers['Authorization']
            verifTokenJWT(tokenCheck)
            return api_method(*args, **kwargs)
        except:
            return 'Error during authentication', 403

    return check_api_key
