from utils.httpResponse import HttpStatus, sendResponse
from flask import request, jsonify
from controllers.itemCtrl import ItemController
from bson.json_util import dumps
from entities.domain.item import ItemSchema

class ItemRoute:

    def __init__(self, mongo):
        self.mongo = mongo
        self.url = '/api/item/'

    def routes(self, app):
        @app.route(f'{self.url}<idItem>', methods=['GET'])
        def findOneItem(idItem: str):
            try:
                item = ItemController.findOneItem(self.mongo, idItem)
                return sendResponse(app, item, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

        @app.route(f'{self.url}', methods=['GET'])
        def findAllItem():
            try:
                items = ItemController.findAllItem(self.mongo)
                return sendResponse(app, items, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

        @app.route(f'{self.url}', methods=['POST'])
        def insertOneItem():
            try:
                itemToCreat = request.get_json(silent=True)
                item = ItemController.insertOneItem(self.mongo, itemToCreat)
                return sendResponse(app, item, HttpStatus.CREATED.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

        @app.route(f'{self.url}<idItem>', methods=['PUT'])
        def updateOneItem(idItem: str):
            try:
                itemToUpdate = request.get_json(silent=True)
                item = ItemController.updateOneItem(self.mongo, itemToUpdate, idItem)
                return sendResponse(app, item, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

