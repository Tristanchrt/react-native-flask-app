from utils.httpResponse import HttpStatus, sendResponse
from flask import request, jsonify
from controllers.userCtrl import UserController
from bson.json_util import dumps
from entities.domain.item import ItemSchema
from functools import wraps
from routes.middleware.tokenVerif import authentication

class UserRoute:

    def __init__(self, mongo):
        self.mongo = mongo
        self.url = '/api/user/'

    def routes(self, app):

        @app.route(f'{self.url}', methods=['GET'])
        # @authentication
        def findAllUser():
            try:
                users = UserController.findAll(self.mongo)
                print(users)
                return sendResponse(app, users, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

        @app.route(f'{self.url}<idUser>', methods=['GET'])
        # @authentication
        def findOneUser(idUser: str):
            try:
                user = UserController.findOne(self.mongo, idUser)
                return sendResponse(app, user, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)

        @app.route(f'{self.url}connect', methods=['POST'])
        def connect():
            try:
                body = request.get_json(silent=True)
                print(body)
                user = UserController.connect(self.mongo, body)
                return sendResponse(app, user, HttpStatus.OK.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)
        
        @app.route(f'{self.url}', methods=['POST'])
        def insertOneUser():
            try:
                user = request.get_json(silent=True)
                user = UserController.insertOne(self.mongo, user)
                return sendResponse(app, user, HttpStatus.CREATED.value)
            except Exception as e:
                return sendResponse(app, e, HttpStatus.BAD_REQUEST.value)