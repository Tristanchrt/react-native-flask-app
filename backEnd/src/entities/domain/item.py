from jsonschema import validate


ItemSchema = {
     "type" : "object",
     "properties" : {
            "title": { "type": "string" },
            "price": { "type": "number" },
            "priceRange": { "type": "object" },
            "amount": { "type": "number" },
            "description": { "type": "string" },
            "completeDescription": { "type": "string" },
            "articlePersonalize": { "type": "boolean" },
            "allMark": { "type": "number" },
            "averageMark": { "type": "number" },
            "images": { "type": "array", "maxItems": 7, "default": [] },
            "createdAt": { "type": "string", "format": "date", "default": "Date.now" },
    },
    "required": ["title", "price", "amount", "articlePersonalize", "description"]
}