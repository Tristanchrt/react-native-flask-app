from jsonschema import validate 

UserSchema = {
    "type" : "object",
    "properties" : {
        "email" : { "type": "string" , "uniqueItems": True },
        "password" : { "type" : "string" },
        "firstName" : { "type" : "string" },
        "lastName" : { "type" : "string" },
        "description" : { "type" : "string" },
        "birthDate": { "type": "string", "format": "date" },
    },
    "required": ["email", "password", "firstName", "lastName"]
}