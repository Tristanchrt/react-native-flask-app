from bson.objectid import ObjectId
from jsonschema import validate
from .error.exception import errorController 

try:
    from ..entities.domain.item import ItemSchema
except:
    from  entities.domain.item import ItemSchema


class ItemController:
    
    @staticmethod
    def findAllItem(mongo):
        try:
            items = mongo.db.items.find({})
            return items
        except Exception as e:
            raise errorController(e, 'Error for finding all items')

    @staticmethod
    def findOneItem(mongo, idItem: str):
        try:
            item = mongo.db.items.find({ "_id": ObjectId(idItem) })
            return item
        except Exception as e:
            raise errorController(e, 'Error for finding item')

    @staticmethod
    def insertOneItem(mongo, item):
        try:
            validate(instance=item, schema=ItemSchema)            
            itemCreated = mongo.db.item.insert_one(item)
            return itemCreated
        except Exception as e:
            raise errorController(e, 'Error for create creation item')
            
    @staticmethod
    def updateOneItem(mongo, item, id: str):
        try:
            validate(instance=item, schema=ItemSchema)            
            itemUpdated = mongo.db.item.update_one({'_id': ObjectId(id) }, { '$set': item }, upsert=False)
            return itemUpdated
        except Exception as e:
            raise errorController(e, 'Error for update item')
