from bson.objectid import ObjectId
from entities.domain.user import UserSchema
from jsonschema import validate
import hashlib
from bson.json_util import dumps
from utils.token import createTokenJWT
import jwt
from .error.exception import errorController 

class UserController:
    
    @staticmethod
    def findAll(mongo):
        try:
            users = mongo.db.userschemas.find({})
            return users
        except Exception as e:
            raise errorController(e, 'Error for finding all users')

    @staticmethod
    def findOne(mongo, idUser: str):
        try:
            user = mongo.db.userschemas.find_one({ "_id": ObjectId(idUser) })
            return user
        except Exception as e:
            raise errorController(e, 'Error for find one user')

    @staticmethod
    def connect(mongo, body):
        try:
            user = mongo.db.userschemas.find_one({ "email": body['email'].lower().replace(" ", "") })
            password = hashlib.sha256(body['password'].encode())
            if user['password'] == password.hexdigest():
                encodedJWT = createTokenJWT(user)
                user['token'] = encodedJWT
                return user
            else:   
                raise errorController("", 'Error during connection')
        except Exception as e:
            raise errorController(e, 'Error during connection')

    @staticmethod
    def insertOne(mongo, user):
        try:
            validate(instance=user, schema=UserSchema)            
            userCreated = mongo.db.userschemas.insert_one(user)
            encodedJWT = createTokenJWT(userCreated)
            user['token'] = encodedJWT
            return userCreated
        except Exception as e:
            raise errorController(e, 'Error for insert user')
