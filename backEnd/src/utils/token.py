import hashlib
import jwt

SECRET_KEY='fmfj84vf41e51e98c5s1dee98487xqv2zazz98v1c2w'


def createTokenJWT(user):
    return jwt.encode(parseUser(user), SECRET_KEY, algorithm="HS256")

def verifTokenJWT(tokenToVerif: str):
    return jwt.decode(tokenToVerif, SECRET_KEY, algorithms=["HS256"], options={"verify_signature": True})

def parseUser(user):
    return { "id" : str(user["_id"]), "firstName" : str(user["firstName"]), "lastName" : str(user["lastName"]) }
