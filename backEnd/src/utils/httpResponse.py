from enum import Enum
from bson.json_util import dumps


class HttpStatus(Enum):
    OK = 200
    CREATED = 201
    BAD_REQUEST = 404
    ERROR_SERVER = 500


def sendResponse(app, jsonValue, HttpStatus):
     return app.response_class(
                        response=dumps(jsonValue),
                        status=HttpStatus,
                        mimetype='application/json'
                    )