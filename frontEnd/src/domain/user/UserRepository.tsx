import { User } from './User';

export interface UserRepositoryInt {
  getUsers(): Promise<User[]>;
  connect(email: string, password: string): Promise<User>;
}
