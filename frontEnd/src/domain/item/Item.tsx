export interface Item {
  id: any;
  title: string;
  price: number;
  amount: number;
  articlePersonalize: boolean;
  description: string;
  images: Array<string> | null;
}
