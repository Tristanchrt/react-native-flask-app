import { Item } from './Item';

export interface ItemRepositoryInt {
  getItems(): Promise<Item[]>;
  getItem(idItem: string): Promise<Item>;
}
