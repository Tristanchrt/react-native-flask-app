import MaskedView from '@react-native-masked-view/masked-view';
import React, { Component, useEffect, useRef } from 'react';
import { Animated, Text, View } from 'react-native';
import { getToken, removeItem } from '../../../secondary/storeRepository/StoreRest';
import { Actions } from 'react-native-router-flux';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { Item } from '../../../domain/item/Item';
import { ItemRepositoryInt } from '../../../domain/item/ItemRepository';
import { ItemRepository } from '../../../secondary/itemRepository/ItemRepository';

interface Props {}

interface State {
  carouselItems: Array<any>;
}

abstract class SliderAbs extends React.Component<Props, State> {
  private itemRepository: ItemRepositoryInt;
  constructor(props: Props) {
    super(props);

    this.itemRepository = new ItemRepository();

    this.state = {
      carouselItems: [],
    };

    this.getItems();
  }

  async getItems() {
    await this.itemRepository
      .getItems()
      .then((response: any) => this.setState({ carouselItems: response }))
      .catch((error: any) => {});
  }

  openPopUp = (item: Item) => {
    console.log(item);
  };
}

export default SliderAbs;
