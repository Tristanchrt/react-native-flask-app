import React, { Component, useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './Slider.style';
import SliderAbs from './Slider.abs';

export class Slider extends SliderAbs {
  render() {
    return (
      <View style={styles.slide}>
        <ScrollView>
          {this.state.carouselItems.map((item: any, index: number) => {
            return (
              <TouchableOpacity key={index} style={styles.item} onPress={() => this.openPopUp(item)}>
                <Text style={styles.title}>{item.id}</Text>
                <Text>{item.title}</Text>
                <Text>{item.amount}</Text>
                <Text>{item.description}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

export default Slider;
