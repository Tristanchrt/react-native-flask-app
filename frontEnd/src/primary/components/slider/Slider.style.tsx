import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  slide: {
    flex: 10,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  item: {
    backgroundColor: '#D0EDFC',
    alignItems: 'center',
    height: 150,
    width: 350,
    marginTop: 15,
    marginBottom: 15,
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },
  title: {
    fontSize: 16,
    marginBottom: 5,
  },
});
