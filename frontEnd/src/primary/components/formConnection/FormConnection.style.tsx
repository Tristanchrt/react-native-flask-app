import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  form: {
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 3,
    height: 40,
    width: 150,
  },
  title: {
    alignItems: 'center',
    marginTop: 15,
  },
  btn: {
    marginTop: 50,
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    textAlign: 'center',
    width: 100,
    alignItems: 'center',
    elevation: 3,
  },
});
