import React from 'react';
import { View, Text, Button, Animated, Image } from 'react-native';
import axios from 'axios';
import { User } from '../../../domain/user/User';
import { UserRepositoryInt } from '../../../domain/user/UserRepository';
import UserRepository from '../../../secondary/userRepository/UserRepository';
import { getToken } from '../../../secondary/storeRepository/StoreRest';
import { Actions } from 'react-native-router-flux';

export interface Props {}

interface State {
  username: string;
  password: string;
}

class FormConnectionAbs extends React.Component<Props, State> {
  private userRepository: UserRepositoryInt;
  constructor(props: Props) {
    super(props);
    this.userRepository = new UserRepository();
    this.state = {
      username: '',
      password: '',
    };
  }

  async onConnection() {
    if (this.state.username != '' && this.state.password != '') {
      await this.userRepository
        .connect(this.state.username, this.state.password)
        .then((response: User) => Actions.itemView())
        .catch((error: any) => {});
    }
  }
}

export default FormConnectionAbs;
