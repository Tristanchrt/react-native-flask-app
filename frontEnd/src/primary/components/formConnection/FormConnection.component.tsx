import ConnectionAbs from './Connection.abs';
import { View, Text, Button, Animated, Image, TextInput, TouchableOpacity } from 'react-native';
import React, { Component, useEffect, useRef } from 'react';
import FormConnectionAbs from './FormConnection.abs';
import { styles } from './FormConnection.style';

export class FormConnection extends FormConnectionAbs {
  render() {
    return (
      <View>
        <View style={styles.form}>
          <Text style={styles.title}>Email : </Text>
          <TextInput
            onChangeText={(value) => this.setState({ username: value })}
            style={styles.input}
            placeholder="Email..."
          ></TextInput>
          <Text style={styles.title}>Password : </Text>
          <TextInput
            secureTextEntry={true}
            onChangeText={(value) => this.setState({ password: value })}
            style={styles.input}
            placeholder="Password..."
          ></TextInput>
          <TouchableOpacity style={styles.btn} onPress={() => this.onConnection()}>
            <Text>CONNEXION</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default FormConnection;
