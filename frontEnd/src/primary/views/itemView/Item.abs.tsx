import MaskedView from '@react-native-masked-view/masked-view';
import React, { Component, useEffect, useRef } from 'react';
import { Animated } from 'react-native';
import { getToken, removeItem } from '../../../secondary/storeRepository/StoreRest';
import { Actions } from 'react-native-router-flux';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

interface Props {}

interface State {}

abstract class ItemAbs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};
  }
  async removeItem() {
    removeItem().then((value) => Actions.home());
  }
  openMenu() {
    console.log('test');
  }
}

export default ItemAbs;
