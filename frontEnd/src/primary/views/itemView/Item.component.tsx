import React, { Component, useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { styles } from './Item.style';
import ItemAbs from './Item.abs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { MenuItem } from '../utils/MenuItem.component';
import Slider from '../../components/slider/Slider.component';

export class ItemView extends ItemAbs {
  render() {
    const homeScreen = ({ navigation }: any) => {
      return (
        <View style={styles.container}>
          <MenuItem navigation={navigation} />
          <Slider />
        </View>
      );
    };

    const profilPage = ({ navigation }: any): any => {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <MenuItem navigation={navigation} />
          <View style={styles.item}>
            <Text>Profile page</Text>
            <TouchableOpacity style={styles.btn} onPress={() => this.removeItem()}>
              <Text>Deconnect</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    };

    const MenuScreen = (props: any): any => {
      return (
        <DrawerContentScrollView {...props}>
          <DrawerItemList {...props} />
        </DrawerContentScrollView>
      );
    };

    const Drawer: any = createDrawerNavigator();

    const ViewItem = (): any => {
      return (
        <Drawer.Navigator drawerContent={(props: any) => <MenuScreen {...props} />}>
          <Drawer.Screen name="Article" component={homeScreen} />
          <Drawer.Screen name="Profile" component={profilPage} />
        </Drawer.Navigator>
      );
    };

    return (
      <NavigationContainer>
        <ViewItem />
      </NavigationContainer>
    );
  }
}

export default ItemView;
