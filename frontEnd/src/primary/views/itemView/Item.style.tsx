import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  menubar: {
    width: 100,
    height: 100,
    backgroundColor: 'red',
  },
  item: {
    flex: 10,
    alignItems: 'center',
  },
  btn: {
    marginTop: 50,
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    textAlign: 'center',
    width: 100,
    alignItems: 'center',
    elevation: 3,
  },
});
