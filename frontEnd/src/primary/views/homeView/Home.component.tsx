import React, { Component, useEffect, useRef } from 'react';
import { View, Text, Button, Animated, MaskedViewIOS, Image, StyleSheet } from 'react-native';
import { styles } from './Home.style';
import HomeAbs from './Home.abs';
import Connection from '../connectionView/Connection.component';
import ItemView from '../itemView/Item.component';

export class Home extends HomeAbs {
  render() {
    const loadingRenderView = this.state.animateDone ? null : (
      <View style={[StyleSheet.absoluteFill, styles.centeredBefore]}>
        <Image
          style={styles.imageCenter}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
      </View>
    );
    const renderView = this.state.animateDone ? this.state.isConnected == true ? <ItemView /> : <Connection /> : null;

    return (
      <View style={styles.container}>
        {loadingRenderView}
        {renderView}
      </View>
    );
  }
}

export default Home;
