import MaskedView from '@react-native-masked-view/masked-view';
import React, { Component, useEffect, useRef } from 'react';
import { Animated } from 'react-native';
import { getToken } from '../../../secondary/storeRepository/StoreRest';

export interface Props {}

interface State {
  animateDone: boolean;
  isConnected: boolean;
  loadingProgress: any;
}

abstract class HomeAbs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      animateDone: false,
      isConnected: false,
      loadingProgress: new Animated.Value(0),
    };
    this.fadeIn();
    this.connected();
  }
  fadeIn() {
    Animated.timing(this.state.loadingProgress, {
      toValue: 100,
      duration: 500,
      useNativeDriver: false,
      delay: 100,
    }).start(() => {
      this.setState({ animateDone: true });
    });
  }
  connected() {
    getToken().then((value) => {
      if (value != null) this.setState({ isConnected: true });
    });
  }
}

export default HomeAbs;
