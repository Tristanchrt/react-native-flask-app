import React from 'react';
import { View, Text, Button, Animated, Image } from 'react-native';

export interface Props {}

interface State {}

abstract class ConnectionAbs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};
  }
}

export default ConnectionAbs;
