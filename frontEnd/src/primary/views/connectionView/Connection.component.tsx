import ConnectionAbs from './Connection.abs';
import { View, Text, Button, Animated, Image } from 'react-native';
import React, { Component, useEffect, useRef } from 'react';
import { styles } from './Connection.style';
import FormConnection from '../../components/formConnection/FormConnection.component';

export class Connection extends ConnectionAbs {
  render() {
    return (
      <View style={styles.centered}>
        <Text style={styles.title}>Connexion à l'application </Text>
        <Text style={[styles.title, { marginBottom: 65 }]}>ITEMS GESTION</Text>
        <FormConnection />
      </View>
    );
  }
}

export default Connection;
