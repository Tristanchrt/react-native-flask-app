import React, { Component, useEffect, useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';

export const styles = StyleSheet.create({
  menu: {
    flex: 1,
    backgroundColor: '#34B2F1',
    height: 40,
    marginTop: 0,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  icon: {
    marginTop: 10,
  },
});

interface Props {
  navigation: any;
}

interface State {}

export class MenuItem extends React.Component<Props, State> {
  navigation: any;
  constructor(props: Props) {
    super(props);
    this.navigation = props.navigation;

    this.state = {};
  }
  render() {
    return (
      <View style={styles.menu}>
        <Icon name="menu" size={40} containerStyle={styles.icon} onPress={() => this.navigation.toggleDrawer()} />
        <Icon name="face" size={40} containerStyle={styles.icon} />
        <Icon name="menu" size={40} containerStyle={[styles.icon, { opacity: 0 }]} />
      </View>
    );
  }
}
