import React, { Component, useEffect, useRef } from 'react';

import AppAbs from './App.abs';
import { Connection } from '../views/connectionView/Connection.component';
import { Router, Scene } from 'react-native-router-flux';
import Home from '../views/homeView/Home.component';
import ItemView from '../views/itemView/Item.component';

export class App extends AppAbs {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="home" component={Home} hideNavBar={true} title="Home" initial={true} />
          <Scene key="connectionView" component={Connection} hideNavBar={true} title="Connection" />
          <Scene key="itemView" component={ItemView} hideNavBar={true} title="Item" />
        </Scene>
      </Router>
    );
  }
}

export default App;
