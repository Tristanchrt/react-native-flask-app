import MaskedView from '@react-native-masked-view/masked-view';
import React, { Component, useEffect, useRef } from 'react';
import { Animated } from 'react-native';

export interface Props {}

interface State {}

abstract class AppAbs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};
  }
}

export default AppAbs;
