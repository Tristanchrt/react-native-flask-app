import axios, { AxiosInstance } from 'axios';

export const axiosConnect = () => {
  return axios.create({
    baseURL: 'http://192.168.1.140:5000/api/',
  });
};
