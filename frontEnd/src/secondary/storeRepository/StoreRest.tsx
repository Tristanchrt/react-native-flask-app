import AsyncStorage from '@react-native-community/async-storage';

export const setToken = async (data: string) => {
  try {
    await AsyncStorage.setItem('token', data);
  } catch (error) {
    throw new Error('Error set token');
  }
};

export const getToken = async (): Promise<any> => {
  try {
    const value = await AsyncStorage.getItem('token');
    return value;
  } catch (error) {
    throw new Error('Error get token');
  }
};

export const removeItem = async (): Promise<any> => {
  try {
    const value = await AsyncStorage.removeItem('token');
    return value;
  } catch (error) {
    throw new Error('Error get token');
  }
};
