import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Item } from '../../domain/item/Item';
import { ItemRepositoryInt } from '../../domain/item/ItemRepository';
import { axiosConnect } from '../utils/axios';
import { RestItem, toItem } from './ItemRest';

export class ItemRepository implements ItemRepositoryInt {
  axiosInstance: AxiosInstance;
  baseUrl: string;
  constructor() {
    this.axiosInstance = axiosConnect();
    this.baseUrl = `item/`;
  }
  async getItems(): Promise<Item[]> {
    return this.axiosInstance
      .get(this.baseUrl)
      .then((response: AxiosResponse<RestItem[]>) => {
        return response.data.map((resp) => toItem(resp));
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }
  async getItem(idItem: string): Promise<Item> {
    return this.axiosInstance
      .get(`${this.baseUrl}idItem`)
      .then((response: AxiosResponse<RestItem>) => {
        return toItem(response.data);
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }
}
