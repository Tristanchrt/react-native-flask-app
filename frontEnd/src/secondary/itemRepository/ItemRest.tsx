import { Item } from '../../domain/item/Item';

export interface RestItem {
  _id: any;
  title: string;
  price: number;
  priceRange?: Record<string, any>;
  amount: number;
  description: string;
  completeDescription?: string;
  articlePersonalize: boolean;
  allMark?: number;
  averageMark?: number;
  images: Array<string>;
  createdAt?: Date;
}

export const toItem = (item: RestItem): Item => ({
  id: item._id.$oid,
  title: item.title,
  price: item.price,
  amount: item.amount,
  articlePersonalize: item.articlePersonalize,
  description: item.description,
  images: item.images ? item.images : null,
});
