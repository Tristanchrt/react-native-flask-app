import axios, { AxiosInstance, AxiosResponse } from 'axios';
import React from 'react';
import { User } from '../../domain/user/User';
import { UserRepositoryInt } from '../../domain/user/UserRepository';
import { setToken } from '../storeRepository/StoreRest';
import { axiosConnect } from '../utils/axios';
import { RestUser, toUser } from './UserRest';

export class UserRepository implements UserRepositoryInt {
  axiosInstance: AxiosInstance;
  baseUrl: string;
  constructor() {
    this.axiosInstance = axiosConnect();
    this.baseUrl = `user/`;
  }
  async getUsers(): Promise<User[]> {
    return this.axiosInstance
      .get(this.baseUrl)
      .then((response: AxiosResponse<RestUser[]>) => {
        return response.data.map((resp) => toUser(resp));
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }
  async connect(email: string, password: string): Promise<User> {
    return this.axiosInstance
      .post(`${this.baseUrl}connect`, {
        email: email,
        password: password,
      })
      .then((response: AxiosResponse<RestUser>) => {
        setToken(response.data.token!);
        return { ...toUser(response.data), token: response.data.token };
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }
}

export default UserRepository;
