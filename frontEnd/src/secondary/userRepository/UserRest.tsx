import { User } from '../../domain/user/User';

export interface RestUser {
  _id: any | string;
  email: string;
  password: string;
  token?: string;
  firstName: string;
  lastName: string;
  description: string;
  birthDate: Date;
  createdAt: Date;
  __v: number;
}

export const toUser = (user: RestUser): User => ({
  id: user._id.$oid,
  email: user.email,
  firstName: user.firstName,
  lastName: user.lastName,
  description: user.description,
});
